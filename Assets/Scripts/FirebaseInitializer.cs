﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Firebase;

public static class FirebaseInitializer {
    private static List<Action<DependencyStatus>> initializedCallbacks = new List<Action<DependencyStatus>>();
    private static DependencyStatus _dependencyStatus;

    public static bool Initialized { get; private set; } = false;

    public static void Initialize(Action<DependencyStatus> callback = null) {
        if (callback != null) Debug.LogError("Putting a callback here doesn't do anything - Check FirebaseInitializer.Initialized in an Update loop instead (or create a new function here)"); // Removed callback to update Firebase code - Dan G Oct 2021
            
        Debug.LogError("RyanCrash - Straight Initialize init");
        try
        {
            Debug.LogError("RyanCrash - Pre checkDependenciesAsync");
            Task checkDependenciesAsyncResp = FirebaseApp.CheckDependenciesAsync().ContinueWith(checkTask => {
                // Peek at the status and see if we need to try to fix dependencies.
                Debug.LogError($"RyanCrash - checkdependenciesasync found {checkTask.Result}");
                DependencyStatus status = checkTask.Result;
                if (status != DependencyStatus.Available) {
                    Debug.LogError("RyanCrash - status not available");
                    return FirebaseApp.FixDependenciesAsync().ContinueWith(t => {
                        Debug.LogError("RyanCrash - Post fix dependencies async");
                        return FirebaseApp.CheckDependenciesAsync();
                    }).Unwrap();
                }

                Debug.LogError("RyanCrash - Dependencies were available!");
                return checkTask;
            }).Unwrap().ContinueWith(task => {
                Debug.LogError("RyanCrash - Post CheckDependenciesAsync");
                _dependencyStatus = task.Result;
                if (_dependencyStatus == DependencyStatus.Available) {
                    lock (initializedCallbacks)
                    {
                        _dependencyStatus = task.Result;
                        Initialized = true;
                        Debug.LogError("RyanCrash - Dependencies are good! Calling callbacks");
                        Debug.LogWarning("Firebase dependencies checked, fixed & initialized");
                        CallInitializedCallbacks();
                    }
                } else {
                    Debug.LogError(
                        "RyanCrash - Could not resolve all Firebase dependencies: " + _dependencyStatus);
                }
            });
            Debug.LogError("RyanCrash - Outside CheckDependenciesAsync");
            Debug.LogError($"RyanCrash - CheckDependenciesAsync Exception message: {checkDependenciesAsyncResp.Exception?.Message} - Task status: {checkDependenciesAsyncResp.Status} - Task is faulted? {checkDependenciesAsyncResp.IsFaulted}");
          
        }
        catch (Exception)
        {
            Debug.LogError("RyanCrash: CHECKANDFIXDEPENDENCIESASYNC __FAILED__");
        }
    }

    private static void CallInitializedCallbacks() {
        lock (initializedCallbacks) {
            foreach (var callback in initializedCallbacks) {
                callback(_dependencyStatus);
            }
            initializedCallbacks.Clear();
        }
    }

}