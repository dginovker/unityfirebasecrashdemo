# Android Firebase Crash Demo

Minimum reproduction of Firebase crash when the unitypackage is installed outside Assets/

Bug Info:
* Application runs in Editor without warnings or issues
* APK crashes on launch
* New since 8.1.0

Build Info:
* Firebase Storage 8.4.0
* Editor 2019.4.32f1, build settings set to Android
* APK built on MacOS 11.6
* Keystore password is "password"

Project Info:
* Camera has script that initializes Firebase
* Firebase's Storage unitypackage was installed in Assets/PretendSubmodule, rather than Assets/

Notes:
* In our production app, the crash is slightly different than the one here (it crashes on first launch but subsequent launches are fine)
* Our production app had the issue on Editor version 2019.4.28f1, APKs built from Ubuntu, MacOS and Windows
